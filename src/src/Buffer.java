package src;

import java.util.LinkedList;
import java.util.Queue;

public class Buffer {

	private int disponible; 
	private Queue<Mensaje> colaMensajes; 
	private int numClientes; 

	public Buffer(int tamanio, int pNumClientes) 
	{
		numClientes = pNumClientes; 
		disponible = tamanio; 
		colaMensajes =  new LinkedList<Mensaje>();
	}

	public synchronized boolean enviar(Mensaje nuevo) throws InterruptedException
	{
		this.notifyAll();
		
		if (disponible > 0)
		{
			colaMensajes.add(nuevo);
			//System.out.println("ya encole el: "+ nuevo.getContenido());
			disponible --;
			return true; 
		}
		else
		{
			return false; 
		}
	}

	public synchronized Mensaje recibir() throws InterruptedException
	{
		disponible ++;
		return colaMensajes.poll();
	}

	public synchronized void termine() 
	{
		numClientes--;
		if (numClientes == 0)
		{
			this.notifyAll();
		}
	}
	
	public boolean matarServidores()
	{
		if (numClientes==0)
		{
			return true;
		}
		else
		{
			return false; 
		}
	}
}

