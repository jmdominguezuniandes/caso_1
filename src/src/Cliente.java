package src;

public class Cliente extends Thread {

	private int numMensajes; 
	private int id; 
	private Buffer buffer; 

	public Cliente(int pNum,int  pId, Buffer pBuffer)
	{
		numMensajes = pNum; 
		id = pId; 
		buffer = pBuffer; 		
	}

	public void run()
	{
		for ( int i= 0; i < numMensajes ; i++ )
		{
			Mensaje nuevo = new Mensaje(i, id); 

			try 
			{
				while(!buffer.enviar(nuevo));

				synchronized(nuevo)
				{
					nuevo.wait();
				}
				System.out.println("Salida = " + nuevo.getRespuesta());
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		yield();
		
		buffer.termine();
	}

	public int getNumMensajes() {
		return numMensajes;
	}

	public void setNumMensajes(int numMensajes) {
		this.numMensajes = numMensajes;
	}

	public int getIdC() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
