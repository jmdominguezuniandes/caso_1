package src;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main 
{
	
	public static int cuantasImpresas; 

	public static void main(String[] args) 
	{
		cuantasImpresas=0; 

		try {
			leer("data/archivo.txt");
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public static void leer(String archivo) throws FileNotFoundException, IOException 
	{
		String cadena;
		int numeroClientes;
		int numeroServidores;
		String[] consultas;
		int tamanoBuffer;
		
		FileReader f = new FileReader(archivo);
		BufferedReader b = new BufferedReader(f);
		
		cadena = b.readLine();
		String detalles[] = cadena.split(",");
		numeroClientes = Integer.valueOf(detalles[0]);
		numeroServidores = Integer.valueOf(detalles[1]);
		consultas = detalles[2].split(";");
		tamanoBuffer = Integer.valueOf(detalles[3]);
		
		Buffer buffer = new Buffer(tamanoBuffer, numeroClientes);
		
		for(int i = 0; i < numeroClientes; i++)
		{
			Cliente c = new Cliente(Integer.valueOf(consultas[i]), i+1, buffer);
			c.start();
		}
		
		for(int i = 0; i < numeroServidores; i++)
		{
			Servidor s = new Servidor(i+1, buffer);
			s.start();
		}
		
		b.close();
		f.close();
	}
}