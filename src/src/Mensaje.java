package src;

public class Mensaje
{

	
	private int contenido; 
	private int id; 
	private int idCliente; 
	private int respuesta;
	
	
	public Mensaje(int i, int pIdCliente) 
	{
		contenido = (int) (Math.random() * 10); 
		id = i; 
		idCliente = pIdCliente; 	
		System.out.println("Entrada = " + contenido);
	}

	
	public int getContenido() {
		return contenido;
	}


	public void setContenido(int contenido) {
		this.contenido = contenido;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getIdCliente() {
		return idCliente;
	}


	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}


	public int getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}
	
	
	

}
