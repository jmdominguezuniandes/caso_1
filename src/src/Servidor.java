package src;

public class Servidor extends Thread 
{

	private int id;
	private Buffer buffer;

	public Servidor(int id, Buffer pBuffer) 
	{
		this.id = id;
		buffer = pBuffer;
	}

	public void run()
	{
		Mensaje msj = null;

		while(!buffer.matarServidores())
		{
			try 
			{
				msj = buffer.recibir();
				
				if(msj == null)
				{
					synchronized(buffer)
					{
						buffer.wait();
					}
				}
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}


			if (msj != null)
			{
				Main.cuantasImpresas++;
				//System.out.println("cuantas he impreso ? .... pues: "+Main.cuantasImpresas);
				
				msj.setRespuesta(msj.getContenido() + 1);

				synchronized(msj)
				{
					msj.notify();
				}
			}
		}
	}

	public int getIdS() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Buffer getBuffer() {
		return buffer;
	}

	public void setBuffer(Buffer buffer) {
		this.buffer = buffer;
	}
}
